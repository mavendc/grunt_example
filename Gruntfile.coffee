module.exports = (grunt) ->
  
  # Project configuration.
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")
    compress:
      main:
        options:
          mode: "zip"
          archive: "target/go_go_gadget_grunt.zip"
        expand: true
        cwd: "config_stuff/"
        src: ["**/*"]

  
  # Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks "grunt-contrib-compress"
  
  # Default task(s).
  grunt.registerTask "default", ["compress"]
  return